var Discord = require('discord.io');
var fs = require('fs');
var shell = require('shelljs');
var logger = require('winston');
var auth = require('./auth.json');
var config = require('./config.json');


var CmdsList = `Commands:
\`/help\` - Display this page.
\`/status <server>\` - Check whether a gameserver is running or not.
\`/ping\` - Ping the bot.
`

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {colorize: true});
logger.level = 'debug';

// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

// Run once the bot is started
bot.on('ready', function (evt) {
    logger.info(`Connected as: ${bot.username} - (${bot.id})`);
    bot.setPresence({
        game: {name:'Say /help'}
    });
});

//Run when the bot receives a message
bot.on('message', function (user, userID, channelID, message, evt) {
    if (message.substring(0, 1) == '/') { //Catch messages beginning with a "!"
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        args = args.splice(1);
        switch(cmd) {
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
                break;
            case 'status':
                var makeStatus = false;
                switch(args.length) {
                    case 0:
                        var status = getServerStatus(bot.channels[channelID].name);
                        if (status == "fake") { //Channel isn't a gameserver channel
                            statusMsg = "Error: Please specify a server. Correct usage: `!status <server>`. See channel names for possible servers.";
                        }
                        else makeStatus = true; //Channel is a gameserver channel; can infer from context
                        break;
                    case 1:
                        status = getServerStatus(args[0]);
                        makeStatus = true;
                        break;
                    default:
                        statusMsg = "Error: too many arguments. Correct usage: `!status <server>`. See channel names for possible servers.";
                }
                if (makeStatus) {
                    switch(status) {
                        case "fake":
                            statusMsg = "❓ Error: no server found by that name.";
                            break;
                        case "inactive":
                            statusMsg = "🅾 Server is offline.";
                            break;
                        case "active":
                            statusMsg = "✅ Server is online.";
                            break;
                        case "failed":
                            statusMsg = "⛔ Server has crashed. Please wait for an administrator.";
                            break;
                        default:
                            statusMsg = `This message should not appear. Contact Resin. Status: ${status}`

                    }
                }
                bot.sendMessage({
                    to: channelID,
                    message: statusMsg
                });
                break;
            case 'help':
                bot.sendMessage({
                    to: channelID,
                    message: CmdsList
                });
                break;
            default:
                bot.sendMessage({
                    to: channelID,
                    message: "Unknown command."
                });
         }
     }
});

//Gets systemctl is-active for a server (ambiguous names can be used)
const getServerStatus = function(serverName) {
    serverID = config.serverAliases[serverName];
    if(fs.existsSync(`${config.gameserverDir}/${serverID}`)) {
		status = shell.exec(`systemctl is-active gameserver@${serverID}.service`).toString().trim();
	}
    else {
		status = 'fake';
	}
    return status;
};

//
if (config.harass) {
    const harassInterval = setInterval(function() {
        for (let [name, userID] of Object.entries(config.naughtyList)) {
            if (bot.users[userID].game) {
                if (config.bannedGames.indexOf(bot.users[userID].game.name) > -1) {
                    logger.info(`User ${bot.users[userID].username}#${bot.users[userID].id} caught playing ${bot.users[userID].game.name}!`);
                    bot.sendMessage({
                        to: userID,
                        message: `Stop playing ${bot.users[userID].game.name}!`
                    });
                }
            }
        }
    }, config.harassInterval);
}
