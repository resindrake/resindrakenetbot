# ResinDrakeNet bot
This is the bot for the ResinDrakeNet server. Currently it supports the following commands:

* `!help` - Display help.
* `!status <server>` - Check whether a gameserver is running or not.
* `!ping` - Ping the bot.

`<server>` can be any *common, one-word* alias for a server; it will be mapped accordingly. E.g. `gm`, `gmod`, `garrysmod` are all acceptable.

Servers are checked in realtime with the shell command `systemctl is-enabled gameserver@<servername>`.
